package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;


/**
 * The persistent class for the cvterm database table.
 * 
 */
public class Cvterm implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer cvtermId;

	private Integer dbxrefId;

	private String definition;

	private Integer isObsolete;

	private Integer isRelationshiptype;

	private String name;

	private Cv cv;

	public Cvterm() {
	}
	
	public Cvterm(int cvtermId){
		this.cvtermId = cvtermId;
	}

	public Integer getCvtermId() {
		return this.cvtermId;
	}

	public void setCvtermId(Integer cvtermId) {
		this.cvtermId = cvtermId;
	}

	public Integer getDbxrefId() {
		return this.dbxrefId;
	}

	public void setDbxrefId(Integer dbxrefId) {
		this.dbxrefId = dbxrefId;
	}

	public String getDefinition() {
		return this.definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public Integer getIsObsolete() {
		return this.isObsolete;
	}

	public void setIsObsolete(Integer isObsolete) {
		this.isObsolete = isObsolete;
	}

	public Integer getIsRelationshiptype() {
		return this.isRelationshiptype;
	}

	public void setIsRelationshiptype(Integer isRelationshiptype) {
		this.isRelationshiptype = isRelationshiptype;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Cv getCv() {
		return this.cv;
	}

	public void setCv(Cv cv) {
		this.cv = cv;
	}
}