package org.cimmyt.ibdbAccess.core.domain;

import java.io.Serializable;


/**
 * The persistent class for the cv database table.
 * 
 */
public class Cv implements Serializable {
	private static final long serialVersionUID = 1L;

	private int cvId;

	private String definition;

	private String name;

	public Cv() {
	}

	public int getCvId() {
		return this.cvId;
	}

	public void setCvId(int cvId) {
		this.cvId = cvId;
	}

	public String getDefinition() {
		return this.definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}