package org.cimmyt.ibdbAccess.web.service;

import java.util.List;

import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
/**
 * Web interface exposing REST methods to consume {@link StudyService} methods
 * @author jarojas
 *
 */
public interface StudyWebService {

    /**
     * Saves a {@link Study} into the model, after validating proper structure of the request
     * @param study study to save
     * @param bindingResult contains potential validation errors
     * @return
     */
	ResponseEntity<Integer> saveStudy(Study study, BindingResult bindingResult);
	
	/**
	 * Returns studies which name contains the argument string
	 * @param pageRequest subset to retrieve for large results
	 * @param studyName name or part of the name of a study
	 * @return a {@link Page} for the list of studies containing the given parameter.
	 */
	//Page<StudySimple> findByName(Pageable pageRequest, String studyName);
	
	/**
	 * Returns the list of {@link OccurrenceSimple} for a given study
	 * @param pageRequest subset to retrieve for large results
	 * @param studyId id of a particular study
	 * @return a {@link Page} for the list of occurrences in a Study.
	 */
	//Page<OccurrenceSimple> findOccurences(Pageable pageRequest, Integer studyId);
	
	/**
	 * Returns the germplasm list of a study
	 * @param studyId the id of the study
	 * @return a list with all of the {@link GermplasmListEntry entries} for the given study,
	 * or an empty list if not found.
	 */
	List<GermplasmListEntry> findStudyGermplasmList(int studyId);

	/**
	 * Returns the design for a specific occurrence.
	 * @param occurrenceId the id of one {@link NdGeolocation} representing an occurrence
	 * @return the design of the requested occurrence
	 */
	List<StudyEntry> findDesign(int ocurrenceId);
}
