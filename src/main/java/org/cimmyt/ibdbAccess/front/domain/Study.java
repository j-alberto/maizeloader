package org.cimmyt.ibdbAccess.front.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * Front end bean for defining a {@link Study}, such as a Nursery or a Trial
 * @author jarojas
 *
 */
public class Study {

	@NotBlank
	private String name;

	private String objective;

	@NotNull
	private Integer startDate;
	
	private Integer endDate;
	
	@NotBlank
	private String description;
	
	@NotEmpty
	@Pattern(regexp="[N|T]")
	private String studyType;
	
	@NotNull
	@NotBlank
	private String tid;
	
	@NotNull
	@Min(1)
	private Integer userId;
	
	/**
	 * Contains additional metadata for a study. The key is the id of the CvTerm for the attribute, the value must be in String form.
	 */
	private Map<Integer,String> extraMetadata;

	@Valid
	@NotNull
	private List<Occurrence> occurrences;
	@NotNull
    private List<GermplasmListEntry> germplasmEntries;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getObjective() {
		return objective;
	}
	public void setObjective(String objective) {
		this.objective = objective;
	}
	public Integer getStartDate() {
		return startDate;
	}
	public void setStartDate(Integer startDate) {
		this.startDate = startDate;
	}
	public Integer getEndDate() {
		return endDate;
	}
	public void setEndDate(Integer endDate) {
		this.endDate = endDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStudyType() {
		return studyType;
	}
	public void setStudyType(String studyType) {
		this.studyType = studyType;
	}

	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}

	public List<Occurrence> getOccurrences() {
		return occurrences;
	}
	public void setOccurrences(List<Occurrence> occurrences) {
		this.occurrences = occurrences;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
    public Map<Integer, String> getExtraMetadata() {
        if(extraMetadata == null)
            extraMetadata = new HashMap<Integer, String>(20);
        return extraMetadata;
    }
    public void setExtraMetadata(Map<Integer, String> extraMetadata) {
        this.extraMetadata = extraMetadata;
    }
    public List<GermplasmListEntry> getGermplasmEntries() {
        return germplasmEntries;
    }
    public void setGermplasmEntries(List<GermplasmListEntry> germplasmEntries) {
        this.germplasmEntries = germplasmEntries;
    }
}
