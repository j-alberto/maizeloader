package org.cimmyt.ibdbAccess.front.domain.term;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;

/**
 * Fixed set of {@link Term}'s that defines types for {@link ProjectRelationship}
 * @author jarojas
 *
 */
public enum RelationshipTerm implements Term{
	A_SUBFOLDER_OF(1140),
	A_STUDY_IN(1145),
	A_DATASET_OF(1150),
	HAS_PROPERTY(1200),
	HAS_METHOD(1210),
	HAS_SCALE(1220);

	Cvterm term;
	
	private RelationshipTerm(int cvtermId){
		this.term = new Cvterm(cvtermId);
	}

	@Override
	public Cvterm asTerm() {
		return term;
	}

	@Override
	public int id() {
		return term.getCvtermId();
	}

}
