package org.cimmyt.ibdbAccess.front.domain;

/**
 * Defines an entry in the germplasm list of a {@link Study}
 * @author jarojas
 *
 */
public class GermplasmListEntry {
	
		private Integer gid;
		private String selHist;
		private String source;
		private String crossName;
		/*Specifies if it's a test entry or a check*/
		private ListEntryType type;
		private Integer entryNum;

		public GermplasmListEntry() {
		}

public GermplasmListEntry( Integer entryNum, Integer gid, String selHist, String source,
				String crossName, ListEntryType type) {
			super();
			this.gid = gid;
			this.selHist = selHist;
			this.source = source;
			this.crossName = crossName;
			this.type = type;
			this.entryNum = entryNum;
		}

		public Integer getGid() {
			return gid;
		}

		public void setGid(Integer gid) {
			this.gid = gid;
		}

		public ListEntryType getType() {
			return type;
		}

		public void setType(ListEntryType type) {
			this.type = type;
		}

		public Integer getEntryNum() {
			return entryNum;
		}

		public void setEntryNum(Integer entryNum) {
			this.entryNum = entryNum;
		}

		public String getSelHist() {
			return selHist;
		}

		public void setSelHist(String selHist) {
			this.selHist = selHist;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getCrossName() {
			return crossName;
		}

		public void setCrossName(String crossName) {
			this.crossName = crossName;
		}
	}
