package org.cimmyt.ibdbAccess.front.domain.term;

import java.util.HashMap;
import java.util.Map;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.front.domain.Categorical;
import org.cimmyt.ibdbAccess.front.domain.CategoricalMegaEnvironment;

/**
 * Fixed set of {@link Term}'s that help to define a {@link NdGeolocation}, that is, a Study instance.
 * This terms have the particularity that 2 records must be stored for them: one containing the text value
 * and other containing the id of the term that represents that same text value.
 * <p>For example: to store the location of an instance, two Terms must be stored in {@link Projectprop}:
 * LOCATIN_NAME and LOCATION_NAME_ID. In consequence, two values are required in {@link NdGeolocationprop}, for
 * example: "EL BATAN" and "9016"; the second one is an Id in the table location for the location CIMMYT - EL BATAN
 * </p> 
 * @author jarojas
 *
 */
public enum GeolocationPropCategoricalTerm implements Term{

	MEGA_ENVIRONMENT(-1, -1, CategoricalMegaEnvironment.class);
	
	Cvterm cvterm;
	Cvterm termReference;
	Class<? extends Categorical> categoricalClass;
	
	private static Map<Integer, GeolocationPropCategoricalTerm> geolocationPropMap = new HashMap<>();
	
	static{
		for (GeolocationPropCategoricalTerm g : GeolocationPropCategoricalTerm.values()) {
			geolocationPropMap.put(g.id(), g);
		}
	}
	/**
	 * Defines categorical properties for Geolocations
	 * @param cvtermId identifier of the geolocation property (e.g. location_name, mega environment)
	 * @param cvTermReferenceId identifier of the term representing the id of the geolocation property
	 *  (e.g. location_name_id, mega environment id)
	 * @param categoricalClass the {@link Categorical} class that will contain the identifier of the catalog
	 * element representing the value for this property (e.g. id 9016 for the location named "CIMMYT - EL BATAN")
	 */
	private GeolocationPropCategoricalTerm(int cvtermId, int cvTermReferenceId, Class<? extends Categorical> categoricalClass){
		this.cvterm = new Cvterm(cvtermId);
		this.termReference = new Cvterm(cvTermReferenceId);
		this.categoricalClass = categoricalClass;
	}

	/**
	 * Returns a {@link Cvterm} representing the id attribute of this categorical Term
	 */
	@Override
	public Cvterm asTerm(){
		return cvterm;
	}
	public Cvterm asReferenceTerm(){
		return termReference;
	}

	public Class<? extends Categorical> getCategoricalClass(){
		return categoricalClass;
	}
	
	public static GeolocationPropCategoricalTerm forId(Integer geolocationPropertyId){
		return geolocationPropMap.get(geolocationPropertyId);
	}

	public static boolean hasGeolocPropTerm(Integer geolocationPropertyId){
		return geolocationPropMap.containsKey(geolocationPropertyId);
	}

	@Override
	public int id() {
		return cvterm.getCvtermId();
	}
		
}
