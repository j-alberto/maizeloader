package org.cimmyt.ibdbAccess.front.domain.term;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;

/**
 * Basic implementation of {@link StandardTerm}, exposing properties to build neccessary {@link Projectprop} records.
 * that builders
 * @author jarojas
 *
 */
public class BasicStandardTerm implements StandardTerm {

	private String caption;
	private String title;
	private Cvterm term = null;
	
	
	@Override
	public Cvterm asTerm() {
		return term;
	}
	
	public BasicStandardTerm(Cvterm term){
	    
	    this.caption =  term.getDefinition();
	    this.title = term.getName();
	    this.term = term;

	}

	@Override
	public int id() {
		return term.getCvtermId();
	}

	@Override
	public String caption() {
		return caption;
	}

	@Override
	public String title() {
		return title;
	}

}
