package org.cimmyt.ibdbAccess.front.domain;

/**
 * Types for {@link GermplasmListEntry}
 * @author jarojas
 *
 */
public enum ListEntryType {
	
	TEST(10170),CHECK(10180), DISEASE_CHECK(10190), STRESS_CHECK(10200);
	
	private int value;
	
	ListEntryType(int value){
		this.value=value;
	}
	
	public int value(){
		return value;
	}
	/**
	 * Returns a {@link ListEntryType} represented with the value provided
	 * @param checkTypeCode the code of a ListEntryType
	 * @return a ListEntryType for given code, or null if it doesn't exist. 
	 */
	public static ListEntryType valueOf(int checkTypeCode){
		for (ListEntryType type : ListEntryType.values()) {
			if(type.value == checkTypeCode)
				return type;
		}
		return null;
	}
}
