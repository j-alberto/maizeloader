package org.cimmyt.ibdbAccess.front.domain;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;

/**
 * Represents the id of a categorical property. Categorical properties have other {@link Cvterm} as values, or
 * even ids for other entities, like location.
 * All categorical variables must have a corresponding class extending this one, to
 * help conversion mechanisms to find the corresponding Id of a text value.
 * @author jarojas
 */
public class AbstractCategorical implements Categorical {

	private String termId;
	/**
	 * Creates new categorical id values
	 * @param termId the id/value of a categorical variable
	 */
	public AbstractCategorical(String termId) {
		super();
		this.termId = termId;
	}


	/**
	 * Returns a Term identifier
	 * @return the term identifier of a categorical variable, in String format
	 */
	@Override
	public String getValue() {
		return termId;
	}

}
