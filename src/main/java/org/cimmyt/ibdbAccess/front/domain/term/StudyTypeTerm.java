package org.cimmyt.ibdbAccess.front.domain.term;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;
import org.cimmyt.ibdbAccess.front.domain.Study;

/**
 * Fixed types for {@link Study}
 * @author jarojas
 *
 */
public enum StudyTypeTerm implements Term{
	TRIAL(10010),
	NURSERY(10000);

	Cvterm term;
	
	private StudyTypeTerm(int cvtermId){
		this.term = new Cvterm(cvtermId);
	}

	@Override
	public Cvterm asTerm() {
		return term;
	}
	
	public static StudyTypeTerm forId(int id){
		if(id == TRIAL.term.getCvtermId()){
			return StudyTypeTerm.TRIAL;
		}else if(id == NURSERY.term.getCvtermId()){
			return NURSERY;
		}else
			return null;
	}

	@Override
	public int id() {
		return term.getCvtermId();
	}

}
