package org.cimmyt.ibdbAccess.front.domain.term;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;

public enum LocationTerm implements Term {

	LOC_NAME(8180), /*as combo*/
	LOC_ABBREVIATION(8189),
	LOC_ID(8190),
	SITE_CODE(8195),
	SITE_NAME(8196),
	OCC_COUNTRY(22616),
	NREP(8131),
	EXPT_DESIGN(8135),
	Occ_Name(22617),
	Occ_Abbr(22618),
	Occ_Cycle(22619),
	Occ_New_Cycle(22620);

	private Cvterm term; 

	private LocationTerm(int termId) {
		this.term = new Cvterm(termId);
	}
	
	@Override
	public Cvterm asTerm() {
		return term;
	}
	
	public int id(){
		return term.getCvtermId();
	}

	/**
	 * Represents a experimental design named <strong>randomized complete block design</strong>.  Found in ontology with:
	 * <pre>
	 * subject 38406: <code>Type of EXPT_DESIGN</code>
	 * type 1190: <code>has value</code>
	 * </pre>
	 */
	public static String EXPT_DESIGN_COMPLETE_BLOCK = "10110";
}
