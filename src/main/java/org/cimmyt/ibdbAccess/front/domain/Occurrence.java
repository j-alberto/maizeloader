package org.cimmyt.ibdbAccess.front.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

/**
 * Instance of a {@link Study}, meaning it share the germplasm list of that {@link Study}, but with it's own
 * attributes and design.
 * @author jarojas
 *
 */
public class Occurrence {

	
	@NotNull
	private Integer number;
	private List<StudyEntry> entries = new ArrayList<>();
//	private String design;
//	private String abbreviation;
//	private String numRep;
	private Map<Integer,String> extraMetadata;


	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public List<StudyEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<StudyEntry> entries) {
		this.entries = entries;
	}

//    public String getDesign() {
//        return design;
//    }
//
//    public void setDesign(String design) {
//        this.design = design;
//    }
//
//    public String getAbbreviation() {
//        return abbreviation;
//    }
//
//    public void setAbbreviation(String abbreviation) {
//        this.abbreviation = abbreviation;
//    }
//
//    public String getNumRep() {
//        return numRep;
//    }
//
//    public void setNumRep(String numRep) {
//        this.numRep = numRep;
//    }
    
    public Map<Integer, String> getExtraMetadata() {
        if(extraMetadata == null)
            extraMetadata = new HashMap<Integer, String>(20);
        return extraMetadata;
    }
    public void setExtraMetadata(Map<Integer, String> extraMetadata) {
        this.extraMetadata = extraMetadata;
    }
}
