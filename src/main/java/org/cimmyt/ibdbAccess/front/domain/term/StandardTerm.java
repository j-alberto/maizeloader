package org.cimmyt.ibdbAccess.front.domain.term;

/**
 * Interface defining types of {@link Term} that need 3 {@link Projectprop} instances to be defined.
 * @author jarojas
 *
 */
public interface StandardTerm extends Term{

	public String caption();
	public String title();
}
