package org.cimmyt.ibdbAccess.front.domain.term;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;

/**
 * Fixed set of {@link Term}'s that help to describe study properties
 * @author jarojas
 *
 */
public enum ProjectPropDescriptorTerm implements Term{
	IBDB_STRUCTURE(1000),
	STUDY_INFO(1010),
	TRIAL_ENV_INFO(1020),
		ENTRY_CODE(1047),
	VARIABLE_DESCRIPTION(1060),
	STANDARD_VAR_ID(1070),
	TRIAL_PLOT_INFO(1155),
	GERMPLASM_DESCRIPTOR(1804),
	STUDY_DETAIL(1805),
	ENVIRONMENT_DETAIL(1806),
	TRIAL_DESIGN(1810),

	ENV_CONDITIONS(10080),
	PLOT_DATA(10090),

	STUDY_PROGRAM(22613),
	STUDY_NEW_CYCLE(22614),
	STUDY_TID(28351);
	
	Cvterm term;
	
	private ProjectPropDescriptorTerm(int cvtermId){
		this.term = new Cvterm(cvtermId);
	}

	@Override
	public Cvterm asTerm() {
		return term;
	}
	
	public int id(){
		return term.getCvtermId();
	}

}
