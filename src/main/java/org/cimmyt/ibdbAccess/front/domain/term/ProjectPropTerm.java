package org.cimmyt.ibdbAccess.front.domain.term;

import org.cimmyt.ibdbAccess.core.domain.Cvterm;

/**
 * Fixed set of {@link Term}'s that represent study properties
 * @author jarojas
 *
 */
public enum ProjectPropTerm implements StandardTerm{

				
		STUDY_NAME_ASSIGNED(8005,"STUDY_NAME", "Study - assigned (DBCV)",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		STUDY_TITLE_ASSIGNED(8007, "STUDY_TITLE", "Study title - assigned (text)",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		STUDY_USER_ID(8020, "STUDY_UID", "ID of the user entering the study data - assigned (DBID)",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		STUDY_OBJECTIVE(8030, "STUDY_OBJECTIVE", "Objective - described (text)",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		START_DATE(8050, "START_DATE", "Start date - assigned (date)",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		END_DATE(8060, "END_DATE", "End date - assigned (date)",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		STUDY_TYPE(8070,"STUDY_TYPE","Study type",
				ProjectPropDescriptorTerm.STUDY_DETAIL),

		DATASET_NAME_ASSIGNED(8150, "DATASET_NAME", "Dataset name",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		DATASET_TITLE_ASSIGNED(8155, "DATASET_TITLE", "Dataset title",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		DATASET_TYPE_ASSIGNED(8160, "DATASET_TYPE", "Dataset type",
				ProjectPropDescriptorTerm.STUDY_DETAIL),
		TRIAL_INSTANCE_ENUMERATED(8170, "TRIAL_INSTANCE", "Trial instance - enumerated (number)",
				ProjectPropDescriptorTerm.ENVIRONMENT_DETAIL),
		
		PLOT_NUMBER(8200, "PLOT_NO", "Field plot - enumerated (number)",
				ProjectPropDescriptorTerm.TRIAL_DESIGN),
		REP_NUMBER(8210, "REP_NO", "Replication - assigned (number)",
				ProjectPropDescriptorTerm.TRIAL_DESIGN),
		ENTRY_NUMBER_ASSIGNED(8230, "ENTRY_NO", "Germplasm entry - enumerated (number)",
				ProjectPropDescriptorTerm.GERMPLASM_DESCRIPTOR),
		ENTRY_GID_ASSIGNED(8240, "GID", "Germplasm identifier - assigned (DBID)",
				ProjectPropDescriptorTerm.GERMPLASM_DESCRIPTOR),
		CROSS(8377, "CROSS", "The pedigree string of the germplasm",
				ProjectPropDescriptorTerm.GERMPLASM_DESCRIPTOR),
		ENTRY_DESIGNATION_ASSIGNED(8250, "DESIGNATION", "Germplasm identifier - assigned (DBCV)",
				ProjectPropDescriptorTerm.GERMPLASM_DESCRIPTOR),
		ENTRY_TYPE_ASSIGNED(8255, "ENTRY_TYPE", "Entry type (test/check)- assigned (type)",
				ProjectPropDescriptorTerm.GERMPLASM_DESCRIPTOR),
		ENTRY_CODE_ASSIGNED(8300, "ENTRY_CODE", "Germplasm ID - Assigned (Code)",
				ProjectPropDescriptorTerm.ENTRY_CODE);
		
		
		Cvterm term, termDescriptor;
		String cvtermId;
		int cvtermTypeId;
		String title;
		String caption;
		
		private ProjectPropTerm(int cvtermId, String title, String caption, ProjectPropDescriptorTerm descriptor){
			this.cvtermId = String.valueOf(cvtermId);
			this.term = new Cvterm(cvtermId);
			this.termDescriptor = descriptor.asTerm();
			this.cvtermTypeId = termDescriptor.getCvtermId();
			this.title = title;
			this.caption = caption;
		}
		
		@Override
		public Cvterm asTerm(){
			return term;
		}
		@Override
		public int id(){
			return term.getCvtermId();
		}
		@Override
		public String caption(){
			return this.caption;
		}
		@Override
		public String title(){
			return this.title;
		}
		
		public Cvterm asTermDescriptor(){
			return termDescriptor;
		}
		public int type(){
			return this.cvtermTypeId;
		}
		
}
