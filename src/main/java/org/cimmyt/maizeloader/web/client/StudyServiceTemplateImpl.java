package org.cimmyt.maizeloader.web.client;

import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.maizeloader.util.URIServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class StudyServiceTemplateImpl implements StudyServiceTemplate{

	private static final Logger LOG = LoggerFactory.getLogger(StudyServiceTemplateImpl.class);
	
	
	private URIServiceProvider uriProvider;
	private RestTemplate webClient = new RestTemplate();
	
	public StudyServiceTemplateImpl(){}
	
	@Autowired
	public StudyServiceTemplateImpl(URIServiceProvider uriProvider) {
		super();
		this.uriProvider = uriProvider;
	}

	@Override
	public int saveStudy(Study study){
		int studyId = -1;
		
		LOG.info("Saving study: {}. Entries: {}, Occurrences: {}", study.getName()
				, study.getGermplasmEntries().size()
				, study.getOccurrences().size());
		for (Occurrence occ : study.getOccurrences()) {
			LOG.info("{}, Occ {}, size: {}",study.getName()
				, occ.getNumber()
				, occ.getEntries().size());
		}

		try{
			ResponseEntity<Object> studyResponse = webClient.postForEntity(uriProvider.getStudyURI(), study, Object.class);
			if(studyResponse.getStatusCode().equals(HttpStatus.OK)){
				studyId = Integer.parseInt(studyResponse.getBody().toString());
				LOG.info("saved study {} with id: {}", study.getName(), studyId);
			}else {
				LOG.warn("Unexpected message from server");
			}
		}catch(HttpServerErrorException e){
			LOG.error("Could not save study {}. Reason[server]: {}", study.getName(), e.getMessage());
			LOG.error("Response content: {}", e.getResponseBodyAsString());
		}
		catch(HttpClientErrorException e){
			LOG.error("Could not save study {}. Reason[client]: {}", study.getName(), e.getMessage());
			LOG.error("Response content: {}", e.getResponseBodyAsString());
		}
		
		return studyId;
	}
}
