package org.cimmyt.maizeloader.web.client;

import org.cimmyt.ibdbAccess.front.domain.Study;

public interface StudyServiceTemplate {

	int saveStudy(Study study);
}
