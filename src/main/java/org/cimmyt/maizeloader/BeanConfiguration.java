package org.cimmyt.maizeloader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.cimmyt.maizeloader.conversion.GermplasmListEntryConverter;
import org.cimmyt.maizeloader.conversion.OccurrenceConverter;
import org.cimmyt.maizeloader.conversion.StudyConverter;
import org.cimmyt.maizeloader.conversion.StudyEntryConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

@Configuration
public class BeanConfiguration{
	
	@Value("${file.path}")
	private String pathName;
	@Value("${file.name}")
	private String fileName;

	@Bean
	public Scanner fileScanner() throws FileNotFoundException{
		Scanner scanner = new Scanner(new File(pathName+fileName));
		return scanner;
	}
	
	@Bean
	public ConversionService getConversionService(ApplicationContext applicationContext){
		DefaultConversionService defaultConversionService = new DefaultConversionService();
		
		defaultConversionService.addConverter(new OccurrenceConverter());
		defaultConversionService.addConverter(new StudyConverter());
		defaultConversionService.addConverter(new StudyEntryConverter());
		defaultConversionService.addConverter(new GermplasmListEntryConverter());
		
		return defaultConversionService;
	}

}
