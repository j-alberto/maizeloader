package org.cimmyt.maizeloader.business;

import java.util.HashSet;
import java.util.Set;

import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.maizeloader.back.domain.PseudoEntry;
import org.cimmyt.maizeloader.back.domain.PseudoSite;
import org.cimmyt.maizeloader.back.domain.PseudoStudy;
import org.cimmyt.maizeloader.exception.InvalidStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
/**
 * Builds one study at a time, by receiving dataRows (String arrays) representing 
 * components of a Study. To build a study, dataRows must be feeded using 
 * <code>acceptInCurrentStudy()</code> method, and once the study is complete,
 * can be retrieved with <code>consumeFinishedStudy()</code>
 * The correct states to use this builder methods are:
 * -init() - must be called before any other method, and every time after 
 * invoking complete() if this builder is intended to build further Studies
 * -acceptEntity() can be used as many time as desired after invoking init(), as
 * log as complete() is not called.
 * -complete() returns the last study created and not returned yet to caller, leaving
 * this instance in a unusable state.
 * @author jarojas
 *
 */
@Component
public class StudyBuilder {

	enum State{
		INACTIVE,ACTIVE,STARTED
	};
	
	private Study currentStudy;
	private Occurrence currentSite;
	private PseudoStudy currentPsStudy;
	private PseudoSite currentPsSite;
	private Set<String> entrySet;
	private ConversionService converter;
	private int siteCounter;
	private State state = State.INACTIVE;
	private boolean firstElement = true;
	
	public StudyBuilder(){}
	
	@Autowired
	public StudyBuilder(ConversionService converter) {
		super();
		this.converter = converter;
	}
	
	/**
	 * Ensures {@link StudyBuilder} has the proper initialization before starting to process
	 * a batch of Studies.
	 */
	public void init() throws InvalidStateException{
		if(!this.state.equals(State.INACTIVE)){
			throw new InvalidStateException();
		}
		resetStudyVariables();
		state = State.ACTIVE;
		firstElement = true;
	}
	
	private void resetStudyVariables(){
		siteCounter = 0;
		currentStudy = null;
		currentPsStudy = null;
		currentSite = null;
		currentPsSite = null;
		entrySet = new HashSet<>(100);
	}

	/**
	 * Receives a row to be converted in an entity and added as part of a {@link Study}.
	 * If it detects the row belongs to a different study, EntityBuilder will start a new Study.
	 * The previous constructed study can be retrieved with consumeFinisedStudy() method.
	 * @param dataRow a row to be parsed as an entity part of a study (e.g. a plot, a site, etc.)
	 * @return true if a calling to this method triggered the creation of a new Study
	 */
	public Study acceptEntity(String[] dataRow){
		removeQuotes(dataRow);
		if(!(state.equals(State.ACTIVE) ||
				state.equals(State.STARTED))){
			throw new InvalidStateException();
		}

		Study studyCreated = null;
		studyCreated = defineCurrentStudy(dataRow);
		
		defineCurrentSite(dataRow);
		addEntryToCurrent(dataRow);
		
		state = State.STARTED;
		
		return studyCreated;
	}
	
	private void removeQuotes(String[] dataRow){
		for(int i = 0; i<dataRow.length; i++){
			dataRow[i] = dataRow[i].replace("\"", "");
		}
	}
	
	private Study defineCurrentStudy(String[] dataRow){
		boolean created = false;
		PseudoStudy newPsStudy = new PseudoStudy(dataRow);
		Study lastStudy = null;
		if(!newPsStudy.equals(currentPsStudy)){
			lastStudy = currentStudy;
			resetStudyVariables();
			currentStudy = converter.convert(newPsStudy, Study.class);
			created = !firstElement;
		}
		currentPsStudy = newPsStudy;
		firstElement = false;
		return created ? lastStudy : null;
		
		
		
	}
	
	private void defineCurrentSite(String[] dataRow){
		PseudoSite newPsSite = new PseudoSite(dataRow);
		
		if(!newPsSite.equals(currentPsSite)){
			siteCounter++;
			currentSite = converter.convert(newPsSite, Occurrence.class);
			currentSite.setNumber(siteCounter);
			currentStudy.getOccurrences().add(currentSite);
		}
		currentPsSite = newPsSite;
		
	}
	
	private void addEntryToCurrent(String[] dataRow){
		PseudoEntry newPsEntry = new PseudoEntry(dataRow);
		StudyEntry entry = converter.convert(newPsEntry, StudyEntry.class);
		
		currentSite.getEntries().add(entry);
		addToGermplasmList(dataRow);
		
	}

	private void addToGermplasmList(String[] dataRow){
		String entry = dataRow[19];
		if(!entrySet.contains(entry)){ //entry number
			entrySet.add(entry);
			GermplasmListEntry listEntry = converter.convert(dataRow, GermplasmListEntry.class);
			currentStudy.getGermplasmEntries().add(listEntry);
		}
	}

	public Study complete(){
		if(!this.state.equals(State.STARTED)){
			throw new InvalidStateException();
		}

		Study lastStudy = currentStudy;
		currentStudy = null;
		state = State.INACTIVE;
		
		return lastStudy;
	}
	
}
