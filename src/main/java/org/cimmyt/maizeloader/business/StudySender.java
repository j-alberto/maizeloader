package org.cimmyt.maizeloader.business;

import java.util.Scanner;

import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.maizeloader.web.client.StudyServiceTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class StudySender {
	
	private static final Logger LOG = LoggerFactory.getLogger(StudySender.class);

	private StudyServiceTemplate serviceTemplate;
	private Scanner fileScanner;
	private StudyBuilder studyBuilder;
	private ObjectMapper mapper;
	
	@Value("${web.service.call}")
	private boolean realInvocation = false;
	
	public StudySender(){}
	
	@Autowired
	public StudySender(StudyServiceTemplate serviceTemplate, Scanner fileScanner
			,StudyBuilder studyBuilder
			,MappingJackson2HttpMessageConverter mapperConverter) {
		this.serviceTemplate = serviceTemplate;
		this.fileScanner = fileScanner;
		this.studyBuilder = studyBuilder;
		this.mapper = mapperConverter.getObjectMapper();
	}

	public Integer sendStudies(){
		String headerRow = fileScanner.nextLine();
		LOG.debug("header: {}", headerRow);
		
		Study studyBuilt = null;
		studyBuilder.init();
		while (fileScanner.hasNextLine()) {
			String[] dataRow = fileScanner.nextLine().split(",");
			if(null !=(studyBuilt = studyBuilder.acceptEntity(dataRow))){
				try {
					LOG.info(mapper.writeValueAsString(studyBuilt));
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				if(realInvocation){
					serviceTemplate.saveStudy(studyBuilt);
				}
			}
		}
		if(null !=(studyBuilt = studyBuilder.complete())){
			LOG.info("Sending remaining study: {}", studyBuilt.getName());
			try {
				LOG.info(mapper.writeValueAsString(studyBuilt));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			if(realInvocation){
				serviceTemplate.saveStudy(studyBuilt);
			}
		}
		
		return 0;
	}
}
