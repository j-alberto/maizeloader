package org.cimmyt.maizeloader.util;

import java.net.URI;

import org.cimmyt.ibdbAccess.front.domain.Study;

public interface URIServiceProvider {

	/**
	 * Retrieves the {@link URI} for calling entry services for {@link Study}, HTTP method used (get/post) is determined by
	 * usage
	 * @return URL for calling study services 
	 */
	URI getStudyURI();
}
