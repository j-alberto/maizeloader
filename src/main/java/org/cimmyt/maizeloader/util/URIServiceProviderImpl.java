package org.cimmyt.maizeloader.util;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class URIServiceProviderImpl implements URIServiceProvider {

	@Value("${web.host}")
	private String host;
	
	@Value("${web.port}")
	private String port;
	
	@Value("${web.context}")
	private String rootContext;
	
	private Map<Integer,URI> urls = new HashMap<>();
	private static final int URL_STUDY = 0;
	
	@PostConstruct
	private void initialize(){
		String baseUrl = String.format("http://%s:%s/%s", host, port, rootContext);
		
		urls.put(URL_STUDY, URI.create(baseUrl+"/study"));
	}

	@Override
	public URI getStudyURI() {
		return urls.get(URL_STUDY);
	}

}
