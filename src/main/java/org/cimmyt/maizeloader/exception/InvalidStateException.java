package org.cimmyt.maizeloader.exception;

import org.cimmyt.maizeloader.business.StudyBuilder;

/**
 * Thrown when {@link StudyBuilder}'s methods  are not used in its intended
 * sequence
 * @author jarojas
 *
 */
public class InvalidStateException extends RuntimeException {

	private static final long serialVersionUID = 8530508389379984457L;

	@Override
	public String getMessage() {
		return "trying to use a Study Builder's method in an invalid state. Please check StudyBuilder usage";
	}
}
