package org.cimmyt.maizeloader;

import org.cimmyt.maizeloader.business.StudySender;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class MaizeLoaderApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(MaizeLoaderApplication.class, args);
		
		for (String string : context.getBeanDefinitionNames()) {
			System.out.println("bean: "+string);
		}
		
		StudySender sender = context.getBean(StudySender.class);
		sender.sendStudies();

		
}


}
