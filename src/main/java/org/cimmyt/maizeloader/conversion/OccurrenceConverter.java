package org.cimmyt.maizeloader.conversion;

import java.util.ArrayList;

import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.maizeloader.back.domain.PseudoSite;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class OccurrenceConverter implements Converter<PseudoSite, Occurrence> {

	@Override
	public Occurrence convert(PseudoSite source) {
		Occurrence target = new Occurrence();
		target.setEntries(new ArrayList<StudyEntry>(100));
		
		return target;
	}

}
