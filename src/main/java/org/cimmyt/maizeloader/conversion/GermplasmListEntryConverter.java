package org.cimmyt.maizeloader.conversion;

import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.ListEntryType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class GermplasmListEntryConverter implements Converter<String[], GermplasmListEntry> {

	@Override
	public GermplasmListEntry convert(String[] source) {
		GermplasmListEntry target = new GermplasmListEntry(
				Integer.valueOf(source[19]),
				Integer.valueOf(source[19]),
				source[20],
				"file",
				source[20],
				ListEntryType.TEST);
		
		return target;
	}

}
