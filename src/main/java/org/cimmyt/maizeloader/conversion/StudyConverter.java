package org.cimmyt.maizeloader.conversion;

import java.util.ArrayList;

import org.cimmyt.ibdbAccess.front.domain.GermplasmListEntry;
import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.maizeloader.back.domain.PseudoStudy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StudyConverter implements Converter<PseudoStudy, Study>{

	@Override
	public Study convert(PseudoStudy source) {
		Study target = new Study();
		target.setDescription(source.getField(3));
		target.setEndDate(toIntegerDate(source.getField(8)));
		target.setExtraMetadata(null);
		target.setGermplasmEntries(new ArrayList<GermplasmListEntry>(100));
		target.setName(source.getField(2));
		target.setObjective(null);
		target.setOccurrences(new ArrayList<Occurrence>(20));
		target.setStartDate(toIntegerDate(source.getField(7)));
		target.setStudyType("T");
		target.setTid(source.getField(1));
		target.setUserId(3);
		return target;
	}
	
	private Integer toIntegerDate(String date){
		return Integer.parseInt(
				new StringBuilder()
				.append(date.substring(6, 10))
				.append(date.substring(3, 5))
				.append(date.substring(0, 2))
				.toString());
	}

}
