package org.cimmyt.maizeloader.conversion;

import java.util.HashMap;

import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.maizeloader.back.domain.PseudoEntry;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StudyEntryConverter implements Converter<PseudoEntry, StudyEntry> {

	@Override
	public StudyEntry convert(PseudoEntry source) {
		StudyEntry target = new StudyEntry();
		target.setCol(null);
		target.setEntryNum(Integer.valueOf(source.getField(19)));
		target.setPlot(Integer.valueOf(source.getField(18)));
		target.setRep(Integer.valueOf(source.getField(16)));
		target.setSubBlock(Integer.valueOf(source.getField(17)));
		target.setTraitValues(new HashMap<Integer, String>(20));
		
		return target;
	}

}
