package org.cimmyt.maizeloader.back.domain;

public class PseudoEntry extends PseudoEntity {

	public PseudoEntry(String[] rowEntity) {
		super(rowEntity);
	}

	@Override
	protected int[] defineFieldIndexes() {
		return new int[] {16,17,18,19};
	}

}
