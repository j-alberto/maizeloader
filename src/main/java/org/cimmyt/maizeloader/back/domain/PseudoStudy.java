package org.cimmyt.maizeloader.back.domain;

public class PseudoStudy extends PseudoEntity {

	public PseudoStudy(String[] rowEntity) {
		super(rowEntity);
	}

	@Override
	protected int[] defineFieldIndexes() {
		return new int[] {0,1,2,3};
	}

}
