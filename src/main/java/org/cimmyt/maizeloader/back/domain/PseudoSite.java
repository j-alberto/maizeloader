package org.cimmyt.maizeloader.back.domain;

public class PseudoSite extends PseudoEntity {

	public PseudoSite(String[] rowEntity) {
		super(rowEntity);
	}

	@Override
	protected int[] defineFieldIndexes() {
		return new int[] {5,6,7,8,10,11,12};
	}

}
