package org.cimmyt.maizeloader.back.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that allows comparing entities, when they are still defined as a String array.
 * Subclasses must expose the protected constructor to allow instantiation.
 * @author jarojas
 *
 */
public abstract class PseudoEntity{

	private static final Logger LOG = LoggerFactory.getLogger(PseudoEntity.class);
	
	private String[] rowEntity = null;
	private int[] definingFieldIndexes = null;
	
	protected PseudoEntity(String[] rowEntity){
		this.rowEntity = rowEntity;
		this.definingFieldIndexes = defineFieldIndexes();
		
		if(this.definingFieldIndexes == null || this.definingFieldIndexes.length == 0){
			throw new InstantiationError("cannot construct a pseudo entity with an empty or null definingFieldIndexes ");
		}
	}
	
	/**
	 * defines in which positions of the entityRow are the fields that make a PseudoEntity unique. 
	 * Subclasses must ensure this method returns an array with at least one element, 
	 * otherwise an {@link InstantiationException} will be thrown by constructor.
	 *  
	 * @return indexes/positions of fields that make this entity unique
	 */
	protected abstract int[] defineFieldIndexes();
	
	@Override
	public boolean equals(Object obj) {
		boolean equals = false;
		
		if(obj instanceof PseudoEntity){
			equals = definingFieldsMatch((PseudoEntity)obj);
		}
		
		return equals;
	}
	
	/**
	 * Compares all defining field values of this instance against given instance.
	 * @param comparedEntity entity to compare against
	 * @return true if and only if all values in defining fields match. False otherwise
	 */
	private boolean definingFieldsMatch(PseudoEntity comparedEntity){
		boolean match = true;
		
		for (int i : definingFieldIndexes) {
			String comparedValue = comparedEntity.getField(i);
			if(!rowEntity[i].equals(comparedValue)){
				LOG.debug("different at index {},  {} is not {}", i,rowEntity[i], comparedValue);
				match = false;
				break;
			}
		}
		return match;
	}

	/**
	 * Returns the field value in the position given,
	 * @param index position of the field to look up, starting at 0.
	 * @return the field value, or null if index is out of range.
	 */
	public final String getField(int index){
		return index >= rowEntity.length ? null : rowEntity[index];
	}
}
