package org.cimmyt.maizeloader.web.client;

import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.maizeloader.MaizeLoaderApplication;
import org.cimmyt.maizeloader.web.client.StudyServiceTemplateImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MaizeLoaderApplication.class)
public class StudyServiceTemplateTest {

	@Autowired
	private StudyServiceTemplateImpl studyServiceTemplate;
	
	@Test
	public void canSaveStudy(){
		Study study = new Study();
		study.setName("testClientStudy");
		study.setTid("123456");
		study.setStudyType("T");
		study.setDescription("maize test");
		
		studyServiceTemplate.saveStudy(study);
		
		
	}
}
