package org.cimmyt.maizeloader.test.util;

import java.io.InputStream;
import java.util.Scanner;

import org.cimmyt.ibdbAccess.business.StudySenderTest;

public class ResourceUtil {

	/**
	 * Returns a scanner for the file at the URI specified 
	 * @param fileUri relative path to the file
	 * @return a {@link Scanner} initialized
	 */
	public static final Scanner createScannerForFile(String fileUri){
		InputStream testFileStream = StudySenderTest.class.getResourceAsStream(fileUri);
		return new Scanner(testFileStream);
	}
}
