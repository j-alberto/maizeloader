package org.cimmyt.maizeloader.util;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URI;

import org.cimmyt.maizeloader.util.URIServiceProvider;
import org.cimmyt.maizeloader.util.URIServiceProviderImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(BlockJUnit4ClassRunner.class)
public class URIServiceProviderImplTest {

	private static URIServiceProvider uriProvider = new URIServiceProviderImpl();
	
	@BeforeClass
	public static void setUpClass(){
		ReflectionTestUtils.setField(uriProvider, "host", "testhost");
		ReflectionTestUtils.setField(uriProvider, "port", "9999");
		ReflectionTestUtils.setField(uriProvider, "rootContext", "test-context");
		ReflectionTestUtils.invokeMethod(uriProvider, "initialize");
	}
	
	@Test
	public void  canGetStudyURI(){
		URI expected = URI.create("http://testhost:9999/test-context/study");
		URI actual  = uriProvider.getStudyURI();
		assertThat(actual, equalTo(expected));
	}
	
}
