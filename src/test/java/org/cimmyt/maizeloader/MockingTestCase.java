package org.cimmyt.maizeloader;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.mockito.MockitoAnnotations;

/**
 * Auto initializes test properties annotated with @Mock annotation
 * @author jarojas
 *
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class MockingTestCase {

	@Before
	public void initMocks(){
		MockitoAnnotations.initMocks(this);
	}

}
