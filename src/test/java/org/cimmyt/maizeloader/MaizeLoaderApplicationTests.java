package org.cimmyt.maizeloader;

import org.cimmyt.maizeloader.MaizeLoaderApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MaizeLoaderApplication.class)
public class MaizeLoaderApplicationTests {

	@Autowired
	private ApplicationContext context;
	
	@Test
	public void contextLoads() {
		
		Assert.assertTrue("Context not crated correctly", (context.containsBean("fileScanner")));
	}

}
