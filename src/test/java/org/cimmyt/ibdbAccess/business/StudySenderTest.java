package org.cimmyt.ibdbAccess.business;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Scanner;

import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.maizeloader.MockingTestCase;
import org.cimmyt.maizeloader.business.StudyBuilder;
import org.cimmyt.maizeloader.business.StudySender;
import org.cimmyt.maizeloader.test.util.ResourceUtil;
import org.cimmyt.maizeloader.web.client.StudyServiceTemplate;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StudySenderTest extends MockingTestCase{

	@Mock
	StudyServiceTemplate mockServiceTemplate;
	@Mock
	StudyBuilder mockStudyBuilder;
	@Mock
	Study mockStudy;
	@Mock
	ObjectMapper mapper;
	@Mock
	MappingJackson2HttpMessageConverter mapperConverer;
	
	private StudySender studySender;
	
//	Study[] studyReturns = new Study[245];

	@Before
	public void setUp() throws JsonProcessingException{
//		studyReturns[110] = mockStudy;
//		studyReturns[240] = mockStudy;
		Scanner fileScanner = ResourceUtil.createScannerForFile("/sample3-studies.csv");
		studySender = new StudySender(mockServiceTemplate, fileScanner, mockStudyBuilder, mapperConverer);
		
		when(mockStudyBuilder.acceptEntity(any(String[].class))).thenReturn(null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,

				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				mockStudy,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,

				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				null,null,null,null,null,null,null,null,null,null,
				mockStudy,null,null,null,null,null,null,null,null,null);
		when(mockStudyBuilder.complete()).thenReturn(mockStudy);
		
		when(mapper.writeValueAsString(mockStudy)).thenReturn("{mock study}");
		
		when(mapperConverer.getObjectMapper()).thenReturn(mapper);
	}
	
	@Test
	public void canSendStudies(){
		studySender.sendStudies();
		
		verify(mockServiceTemplate, times(3)).saveStudy(mockStudy);
		verify(mockStudyBuilder, times(315)).acceptEntity(any(String[].class));
		verify(mockStudyBuilder, times(1)).complete();
	}
}
