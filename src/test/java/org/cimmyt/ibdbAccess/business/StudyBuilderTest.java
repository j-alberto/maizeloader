package org.cimmyt.ibdbAccess.business;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.cimmyt.ibdbAccess.front.domain.Occurrence;
import org.cimmyt.ibdbAccess.front.domain.Study;
import org.cimmyt.ibdbAccess.front.domain.StudyEntry;
import org.cimmyt.maizeloader.MockingTestCase;
import org.cimmyt.maizeloader.business.StudyBuilder;
import org.cimmyt.maizeloader.exception.InvalidStateException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.core.convert.ConversionService;

public class StudyBuilderTest extends MockingTestCase{

	@Mock
	private ConversionService mockConverter;
	@Mock
	private Study mockStudy;
	@Mock
	private Occurrence mockSite;// = new Occurrence();
	@Mock
	private StudyEntry mockStudyEntry;
	
	private List<StudyEntry> emptyList = new ArrayList<>();
	
	private final String[] TEST_INPUT_1_1="CIMMYT-Kenya,2014,3WHYB-EL14-4-,3WHYB-EL14-4-2,E,G. Asea,Optimal,04/17/2014,09/14/2014,7.88,UGANDA,SERERE,Eastern and Southern Africa,1.517,33.45,1106,1,1,1,20,CKH122184,62,,,,,,,,,,,1,,,,,,,1.5,,,,,,,63,,,,,3,89,,,,0,,,1050,,,,,,13,2.5,,,,,,,,,,1.5,,,,,,,,,,,,,,36,,,,,,,,,3,200,38,,,,,,,,,,0.8,,,,,,,,,,,,,,,,,,,,62,,,,1,,,,,,,,3,,,,,,1.5,,,,,63,,,,,3,89,0.4,,,,,0,,,0.95,,,,,13,2.5,,,1.06,,,,,,1.5,,,,,,,,,,,,,,,3,200,38,,,,,,,,,,,,,,,,,,,,,,,,,,"
			.split(",");
	private final String[] TEST_INPUT_1_2="CIMMYT-Kenya,2014,3WHYB-EL14-4-,3WHYB-EL14-4-2,E,G. Asea,Optimal,04/17/2014,09/14/2014,7.88,UGANDA,SERERE,Eastern and Southern Africa,1.517,33.45,1106,1,1,2,20,CKH122184,62,,,,,,,,,,,1,,,,,,,1.5,,,,,,,63,,,,,3,89,,,,0,,,1050,,,,,,13,2.5,,,,,,,,,,1.5,,,,,,,,,,,,,,36,,,,,,,,,3,200,38,,,,,,,,,,0.8,,,,,,,,,,,,,,,,,,,,62,,,,1,,,,,,,,3,,,,,,1.5,,,,,63,,,,,3,89,0.4,,,,,0,,,0.95,,,,,13,2.5,,,1.06,,,,,,1.5,,,,,,,,,,,,,,,3,200,38,,,,,,,,,,,,,,,,,,,,,,,,,,"
			.split(",");
	private final String[] TEST_INPUT_2_1="CIMMYT-Kenya2,2014,3WHYB-EL14-4-,3WHYB-EL14-4-2,E,G. Asea,Optimal,04/17/2014,09/14/2014,7.88,UGANDA,SERERE,Eastern and Southern Africa,1.517,33.45,1106,1,1,1,20,CKH122184,62,,,,,,,,,,,1,,,,,,,1.5,,,,,,,63,,,,,3,89,,,,0,,,1050,,,,,,13,2.5,,,,,,,,,,1.5,,,,,,,,,,,,,,36,,,,,,,,,3,200,38,,,,,,,,,,0.8,,,,,,,,,,,,,,,,,,,,62,,,,1,,,,,,,,3,,,,,,1.5,,,,,63,,,,,3,89,0.4,,,,,0,,,0.95,,,,,13,2.5,,,1.06,,,,,,1.5,,,,,,,,,,,,,,,3,200,38,,,,,,,,,,,,,,,,,,,,,,,,,,"
			.split(",");
	private final String[] TEST_INPUT_2_2="CIMMYT-Kenya2,2014,3WHYB-EL14-4-,3WHYB-EL14-4-2,E,G. Asea,Optimal,04/17/2014,09/14/2014,7.88,UGANDA,SERERE,Eastern and Southern Africa,1.517,33.45,1106,1,1,2,20,CKH122184,62,,,,,,,,,,,1,,,,,,,1.5,,,,,,,63,,,,,3,89,,,,0,,,1050,,,,,,13,2.5,,,,,,,,,,1.5,,,,,,,,,,,,,,36,,,,,,,,,3,200,38,,,,,,,,,,0.8,,,,,,,,,,,,,,,,,,,,62,,,,1,,,,,,,,3,,,,,,1.5,,,,,63,,,,,3,89,0.4,,,,,0,,,0.95,,,,,13,2.5,,,1.06,,,,,,1.5,,,,,,,,,,,,,,,3,200,38,,,,,,,,,,,,,,,,,,,,,,,,,,"
			.split(",");
	
	private StudyBuilder studyBuilder;

	@Before
	public void setUp(){
		
		when(mockConverter.convert(any(), eq(Study.class)))
			.thenReturn(mockStudy);
		when(mockConverter.convert(any(), eq(Occurrence.class)))
			.thenReturn(mockSite);
		when(mockConverter.convert(any(), eq(StudyEntry.class)))
			.thenReturn(mockStudyEntry);
		
		when(mockSite.getEntries()).thenReturn(emptyList);
		
		studyBuilder = new StudyBuilder(mockConverter);
	}
	
	@Test(expected=InvalidStateException.class)
	public void failWhenAcceptAndNotInited(){
		studyBuilder.acceptEntity(TEST_INPUT_1_1);
	}

	@Test(expected=InvalidStateException.class)
	public void failWhenCompleteAndNotAccepted(){
		studyBuilder.init();
		studyBuilder.complete();
	}

	@Test(expected=InvalidStateException.class)
	public void failWhenInitAndNotCompleted(){
		studyBuilder.init();
		studyBuilder.acceptEntity(TEST_INPUT_1_1);
		studyBuilder.init();
	}

	@Test(expected=InvalidStateException.class)
	public void failWhenCompleteTwice(){
		studyBuilder.init();
		studyBuilder.acceptEntity(TEST_INPUT_1_1);
		studyBuilder.complete();
		studyBuilder.complete();
	}

	@Test
	public void canAcceptOneRow(){
		studyBuilder.init();
		Study actual = studyBuilder.acceptEntity(TEST_INPUT_1_1);
		assertThat(actual, nullValue());
		
		actual = studyBuilder.complete();
		assertThat(actual, notNullValue());
		
		
		verify(mockConverter, times(1)).convert(any(), eq(Study.class));
		verify(mockConverter, times(1)).convert(any(), eq(Occurrence.class));
		verify(mockConverter, times(1)).convert(any(), eq(StudyEntry.class));
		

	}
	
	@Test
	public void canAcceptTwoRows(){
		studyBuilder.init();
		Study actual = studyBuilder.acceptEntity(TEST_INPUT_1_1);
		assertThat(actual, nullValue());
		
		actual = studyBuilder.acceptEntity(TEST_INPUT_1_2);
		assertThat(actual, nullValue());
		
		actual = studyBuilder.complete();
		assertThat(actual, notNullValue());
		
		
		verify(mockConverter, times(1)).convert(any(), eq(Study.class));
		verify(mockConverter, times(1)).convert(any(), eq(Occurrence.class));
		verify(mockConverter, times(2)).convert(any(), eq(StudyEntry.class));
		

	}
	
	@Test
	public void canCreateTwoStudies(){
		studyBuilder.init();
		Study actual = studyBuilder.acceptEntity(TEST_INPUT_1_1);
		assertThat(actual, nullValue());
		
		actual = studyBuilder.acceptEntity(TEST_INPUT_1_2);
		assertThat(actual, nullValue());

		actual = studyBuilder.acceptEntity(TEST_INPUT_2_1);
		assertThat(actual, notNullValue());

		actual = studyBuilder.acceptEntity(TEST_INPUT_2_2);
		assertThat(actual, nullValue());

		actual = studyBuilder.complete();
		assertThat(actual, notNullValue());
		
		verify(mockConverter, times(2)).convert(any(), eq(Study.class));
		verify(mockConverter, times(2)).convert(any(), eq(Occurrence.class));
		verify(mockConverter, times(4)).convert(any(), eq(StudyEntry.class));
		

	}
	
}
