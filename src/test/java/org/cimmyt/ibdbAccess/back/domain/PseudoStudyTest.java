package org.cimmyt.ibdbAccess.back.domain;

import static org.junit.Assert.*;

import org.cimmyt.maizeloader.back.domain.PseudoStudy;

import static org.hamcrest.Matchers.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class PseudoStudyTest {

	@Test
	public void canIdentifyNotEqualPseudoStudies(){
		PseudoStudy studyA = new PseudoStudy(new String[]{"aa","bb","cc","dd","ee","ff"});
		PseudoStudy studyB = new PseudoStudy(new String[]{"aa","bb","ccc","dd","ee","ff"});
		
		assertThat(studyA, not(equalTo(studyB)));
	}

	@Test
	public void canIdentifyEqualPseudoStudies(){
		PseudoStudy studyA = new PseudoStudy(new String[]{"aa","bb","cc","dd","ee","ff"});
		PseudoStudy studyB = new PseudoStudy(new String[]{"aa","bb","cc","dd","eeee","ffff"});

		assertThat(studyA, equalTo(studyB));
	}
	
	//TODO add tests for bad instantiation
}
